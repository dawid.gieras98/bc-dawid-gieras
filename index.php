<?php

use Project\Model\Order;
use Project\Model\Participant;
use Project\Model\ParticipantList;
use Project\Services\ParticipantSortable;
use Project\Services\ParticipantSorter;

require_once "vendor/autoload.php";

function participantSort(ParticipantSortable $service): void
{
    $list = new ParticipantList(
        new Participant('Manchester United'),
        new Participant('FC Barcelona'),
        new Participant('AC Milan')
    );

    var_dump($list);

    var_dump($service->sortByName($list, Order::ASC));
    var_dump($service->sortByName($list, Order::DESC));
}

participantSort(new ParticipantSorter());