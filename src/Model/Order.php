<?php

declare(strict_types=1);

namespace Project\Model;

enum Order
{
    case ASC;
    case DESC;
}
