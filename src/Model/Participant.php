<?php

declare(strict_types=1);

namespace Project\Model;

class Participant implements ParticipantComparable
{
    public function __construct(
        private readonly string $name
    ){
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function compare(Participant $participant): int
    {
        return strcmp($this->name, $participant->getName());
    }
}
