<?php

declare(strict_types=1);

namespace Project\Model;

interface ParticipantComparable
{
    public function compare(Participant $participant): int;
}
