<?php

declare(strict_types=1);

namespace Project\Model;

class ParticipantList
{
    private array $participants;

    public function __construct(Participant ...$participants)
    {
        $this->participants = $participants;
    }

    public function getParticipants(): array
    {
        return $this->participants;
    }
}
