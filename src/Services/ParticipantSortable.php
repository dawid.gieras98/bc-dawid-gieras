<?php

declare(strict_types=1);

namespace Project\Services;

use Project\Model\Order;
use Project\Model\ParticipantList;

interface ParticipantSortable
{
    public function sortByName(ParticipantList $list, Order $order): ParticipantList;
}
