<?php

namespace Project\Services;

use Project\Model\Order;
use Project\Model\Participant;
use Project\Model\ParticipantList;

class ParticipantSorter implements ParticipantSortable
{
    public function sortByName(ParticipantList $list, Order $order): ParticipantList
    {
        $participants = $list->getParticipants();

        usort($participants, function (Participant $a, Participant $b) use ($order) {
            $cmp = $a->compare($b);

            return $order === Order::ASC ? $cmp : -$cmp;
        });

        return new ParticipantList(...$participants);
    }
}