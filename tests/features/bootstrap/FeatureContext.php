<?php

use Behat\Behat\Context\Context;
use Project\Model\Order;
use Project\Model\Participant;
use Project\Model\ParticipantList;
use Project\Services\ParticipantSortable;
use Project\Services\ParticipantSorter;

class FeatureContext implements Context
{
    private ParticipantList $list;
    private ParticipantSortable $service;
    private ParticipantList $result;

    public function __construct()
    {
    }

    /**
     * @Given I have a ParticipantList containing :participant1, :participant2 and :participant3
     */
    public function iHaveAParticipantListContaining($participant1, $participant2, $participant3): void
    {
        $this->list = new ParticipantList(
            new Participant($participant1),
            new Participant($participant2),
            new Participant($participant3)
        );
    }

    /**
     * @When I sort the list in ascending order using the ParticipantSorter service
     */
    public function iSortTheListInAscendingOrderUsingTheParticipantSorterService(): void
    {
        $this->service = new ParticipantSorter();
        $this->result = $this->service->sortByName($this->list, Order::ASC);
    }

    /**
     * @Then the list should be :participant1, :participant2 and :participant3
     */
    public function theListShouldBe($participant1, $participant2, $participant3): void
    {

        $result = $this->result->getParticipants();

        $resultNames = [];
        $expectedNames = [$participant1, $participant2, $participant3];

        foreach ($result as $r) {
            $resultNames[] = $r->getName();
        }

        foreach ($resultNames as $i => $rn) {
            $expectedName = $expectedNames[$i];

            if ($rn !== $expectedName) {
                throw new Exception('The sorted list does not match the expected result.');
            }
        }

    }

    /**
     * @When I sort the list in descending order using the ParticipantSorter service
     */
    public function iSortTheListInDescendingOrderUsingTheParticipantSorterService(): void
    {
        $this->service = new ParticipantSorter();
        $this->result = $this->service->sortByName($this->list, Order::DESC);
    }
}
