Feature: Sorting Participants

  As a user of the ParticipantSorter service
  I want to sort a list of participants by name in ascending or descending order
  So that I can view the list of participants in the desired order

  Scenario: Sorting participants in ascending order
    Given I have a ParticipantList containing "Manchester United", "FC Barcelona" and "AC Milan"
    When I sort the list in ascending order using the ParticipantSorter service
    Then the list should be "AC Milan", "FC Barcelona" and "Manchester United"

  Scenario: Sorting participants in descending order
    Given I have a ParticipantList containing "Manchester United", "FC Barcelona" and "AC Milan"
    When I sort the list in descending order using the ParticipantSorter service
    Then the list should be "Manchester United", "FC Barcelona" and "AC Milan"
